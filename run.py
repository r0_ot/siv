from src.models import SIV
import argparse

parser = argparse.ArgumentParser(
    description='System Integrity Verifier (SIV)',
    usage='%(prog)s <-i|-v|-h> -D monitor_dir -V verification_file -R report_file [-H hash_function] [--verbose]'
)

parser.add_argument('-i', '--init',
                    dest='init', action='store_true', help='initialization mode')

parser.add_argument('-v', '--verification',
                    dest='verification', action='store_true', help='verification mode')

parser.add_argument('-D', '--monitor', action='store', metavar='monitor_dir', required=True,
                    dest='monitored_dir', type=str, help='path of monitor directory')

parser.add_argument('-V', '--verification-file', action='store', metavar='verification_file', required=True,
                    dest='verification_file', type=str, help='path of verification file')

parser.add_argument('-R', '--report-file', action='store', metavar='report_file', required=True,
                    dest='report_file', type=str, help='path of report file')

parser.add_argument('-H', '--hashing',
                    dest='hashing', type=str, help='Hash function for initialization mode, '
                                                   'if you use in verification mode it will be ignored, because '
                                                   'it restored from verification file.',
                    choices=['md5', 'sha1', 'sha256', 'sha224', 'sha512'])

parser.add_argument('--verbose', default=False,
                    dest='verbose', action='store_true', help='Verbosity')

# Parse arguments
args = parser.parse_args()

# Construct a dictionary from inputs in order to use for SIV model
inputs = dict(
    monitored_dir=args.monitored_dir,
    verification_file=args.verification_file,
    report_file=args.report_file,
    verbose=args.verbose
)

# Check one of switches determined
assert args.init ^ args.verification, 'you should use -i or -v (not both) switch in order to choose mode'

# Update inputs due to selected mode
if args.init:
    """Initialization Mode"""
    assert args.hashing, """In initialization mode (-i/--init) you should determine hash function by -H/--hashing switch
        for more information please refer to help page by run program with -h or --help"""

    # Add hashing function and selected mode
    inputs.update(
        hashing=args.hashing,
        mode=SIV.INIT_MODE
    )

# Run System Integrity Verifier
SIV(**inputs).run()
