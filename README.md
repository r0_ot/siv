#### System Integrity Verifier (SIV)

I've implemented a simple `System Integrity Verifier` using python from scratch.


###### Installation

At first you need to clone the source code from git repository as following:
```bash
$ git clone https://gitlab.com/r0_ot/siv.git
```

now you should change your current directory into `siv` directory and install it by `pip`:
```bash
$ cd siv
$ python3 -m pip install -e .
```

then, you can run it and see the usage:
```bash
$ siv -h
usage: siv <-i|-v|-h> -D monitor_dir -V verification_file -R report_file [-H hash_function] [--verbose]

System Integrity Verifier (SIV)

optional arguments:
  -h, --help            show this help message and exit
  -i, --init            initialization mode
  -v, --verification    verification mode
  -D monitor_dir, --monitor monitor_dir
                        path of monitor directory
  -V verification_file, --verification-file verification_file
                        path of verification file
  -R report_file, --report-file report_file
                        path of report file
  -H {md5,sha1,sha256,sha224,sha512}, --hashing {md5,sha1,sha256,sha224,sha512}
                        Hash function for initialization mode, if you use in
                        verification mode it will be ignored, because it
                        restored from verification file.
  --verbose             Verbosity
```
###### LICENSE [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)