from src.models import FileDescriptorType
from sys import stderr
import pickle as pkl
import time


def load(filename: str):
    """Load a Pickle file

    :param filename: str
        path of file to load
    """
    return pkl.load(open(filename, 'rb'))


class Log:
    TEMPLATE = "[{}] {}"

    @staticmethod
    def base_message(icon: str, message: str, verbose: bool = False):
        """Base Message Generator

        :param icon: str
            icon of showing message

        :param message: str
            message what you want to show

        :param verbose: bool (default = False)
            verbose in console

        """
        data = Log.TEMPLATE.format(icon, message)

        # Verbose in console mode
        verbose and stderr.write("{}\n".format(data))

        return data

    @staticmethod
    def info(message: str, verbose: bool = False) -> str:
        """Information
            [I] message
            I indicates to Information

        :param verbose: bool (default = False)
            verbose in console

        :param message: str
            message that you want to show

        :return: str
        """
        return Log.base_message("I", message, verbose)

    @staticmethod
    def warn(message: str, verbose: bool = False) -> str:
        """Warning
            [W] message
            W indicates to warning

        :param message: str
            message that you want to show

        :param verbose: bool (default = False)
            verbose in console

        :return: str
        """
        return Log.base_message("W", message, verbose)

    @staticmethod
    def error(message: str, verbose: bool = False) -> str:
        """Error
            [E] message
            E indicates to Error

        :param message: str
            message that you want to show

        :param verbose: bool (default = False)
            verbose in console

        :return: str
        """
        return Log.base_message("E", message, verbose)


def check_modification(
        previous: FileDescriptorType,
        current: FileDescriptorType,
        comparison: int,
        verbose: bool = False
) -> str:
    """Check Modification of a File

    :param previous: FileDescriptorType
        previous version of file

    :param current: FileDescriptorType
        current version of file

    :param comparison: int
        result of comparing two version of file

    :param verbose: bool (default = False)
        verbose in console

    :return messages
    """
    message = ''
    CHANGE_TEMPLATE = "(Previous: {}, Current: {})"

    # Check size
    if comparison & FileDescriptorType.C_SIZE:
        message += "{}\n".format(
            Log.warn("Size of file '{}' {}.".format(
                current.full_path, CHANGE_TEMPLATE.format(
                    previous.size, current.size
                )
            ), verbose)
        )

    # Check owner
    if comparison & FileDescriptorType.C_OWNER:
        message += "{}\n".format(
            Log.warn("Owner of file '{}' {}.".format(
                current.full_path, CHANGE_TEMPLATE.format(
                    previous.owner, current.owner
                )
            ), verbose)
        )

    # Check group
    if comparison & FileDescriptorType.C_GROUP:
        message += "{}\n".format(
            Log.warn("Group of file '{}' {}.".format(
                current.full_path, CHANGE_TEMPLATE.format(
                    previous.group, current.group
                )
            ), verbose)
        )

    # Check permission
    if comparison & FileDescriptorType.C_PERMISSION:
        message += "{}\n".format(
            Log.warn("Permission of file '{}' {}.".format(
                current.full_path, CHANGE_TEMPLATE.format(
                    previous.permission, current.permission
                )
            ), verbose)
        )

    # Check last modification
    if comparison & FileDescriptorType.C_DATE:
        message += "{}\n".format(
            Log.warn("LastModification of file '{}' {}.".format(
                current.full_path, CHANGE_TEMPLATE.format(
                    time.ctime(previous.last_modification),
                    time.ctime(current.last_modification),
                    # previous.last_modification, current.last_modification
                )
            ), verbose)
        )

    # Check hash
    if comparison & FileDescriptorType.C_HASH:
        message += "{}\n".format(
            Log.warn("HASH of file '{}' {}.".format(
                current.full_path, CHANGE_TEMPLATE.format(
                    previous.digest, current.digest
                )
            ), verbose)
        )

    return message
