import hashlib
import abc
import os


class Runnable:
    @abc.abstractmethod
    def run(self):
        pass


class Base:
    MD5_HASH = 'md5'
    SHA1_HASH = 'sha1'
    SHA256_HASH = 'sha256'
    SHA224_HASH = 'sha224'
    SHA512_HASH = 'sha512'

    SUPPORTED_HASH = [
        MD5_HASH,
        SHA1_HASH,
        SHA256_HASH,
        SHA224_HASH,
        SHA512_HASH,
    ]

    def __init__(
            self,
            monitored_dir: str,
            verification_file: str,
            report_file: str,
            hashing: str = SHA1_HASH
    ):
        """Base Model of System Integrity Verifier

        :param monitored_dir: str
            path of monitoring directory

        :param verification_file: str
            store result of verification files in this file

        :param report_file: str
            store report of every changes in this file

        :param hashing: str (default: 'sha1')
            Hashing function
            supported values: 'md5', 'sha1', 'sha256', 'sha512', 'sha224'
        """

        # Assign inputs to variables
        self.verification_file: str = os.path.abspath(verification_file)
        self.monitored_dir: str = os.path.abspath(monitored_dir)
        self.report_file: str = os.path.abspath(report_file)
        self.hashing: str = hashing

        # Validate inputs
        self.validate()

        # Select Hash function
        self.hash_function = eval('hashlib.{}'.format(self.hashing))

    def overwrite(self, message: str):
        """Overwriting
            Ask a question for overwriting on a file

        :param message: str
            Show Message
        """
        if input("[*] {}".format(message)).lower().startswith('n'):
            exit(0)

    def verify_monitored(self, monitored_dir: str):
        """Verify Monitored path
            Verify the specified directory exists

        :param monitored_dir: str
            path of monitoring directory
        """
        # Check being directory
        assert os.path.isdir(monitored_dir), 'monitored_dir is not directory'

        # Check directory exists
        assert os.path.exists(monitored_dir), 'monitored_dir not exists'

    def verify_hashing(self, hashing: str):
        """Verify Hashing function
            Verify the specified hashing supported or not

        :param hashing: str
            Hashing name
        """
        assert hashing in self.SUPPORTED_HASH, "hashing invalid, supported hashing: '{}'".format(self.SUPPORTED_HASH)

    def verify_report_file(self, report_file: str):
        """Verify Report File
            Verify the specified file outside of monitored directory or not

        :param report_file: str
            path of report file
        """
        # Check is in monitored_dir
        assert self.monitored_dir not in report_file, 'report_file should not be in monitored_dir'
        os.path.exists(report_file) and self.overwrite(
            "report file '{}' exists, do you want to overwrite it ([Y]es/No) ? ".format(
                report_file
            )
        )

    def validate(self):
        """Validate inputs
            this function validate inputs of __init__
        """
        # Validate Monitored
        self.verify_monitored(self.monitored_dir)

        # Validate Verification File
        self.verify_verification_file(self.verification_file)

        # Validate Report file
        self.verify_report_file(self.report_file)

        # Validate Hashing
        self.verify_hashing(self.hashing)

    @abc.abstractmethod
    def verify_verification_file(self, verification_file: str):
        """Verify Verification File
            Verify the specified file outside of monitored directory or not

        :param verification_file: str
            path of verification file
        """


class InitBase(Base):

    def __init__(
            self,
            monitored_dir: str,
            verification_file: str,
            report_file: str,
            hashing: str = Base.SHA1_HASH
    ):
        """Initialize Base Model

        :param monitored_dir: str
            path of monitoring directory

        :param verification_file: str
            store result of verification files in this file

        :param report_file: str
            store report of every changes in this file

        :param hashing: str (default: 'sha1')
            Hashing function
            supported values: 'md5', 'sha1', 'sha256', 'sha512', 'sha224'
        """
        super(InitBase, self).__init__(monitored_dir, verification_file, report_file, hashing)

    def verify_verification_file(self, verification_file: str):
        """Verify Verification File
            Verify the specified file outside of monitored directory or not

        :param verification_file: str
            path of verification file
        """
        # Check is in monitored_dir
        assert self.monitored_dir not in verification_file, 'verification_file ' \
                                                            'should not be in monitored_dir'
        os.path.exists(verification_file) and self.overwrite(
            "verification file '{}' exists, do you want to overwrite it ([Y]es/No) ? ".format(
                verification_file
            )
        )

    def __str__(self):
        return "Monitoring Directory: '{}'\n" \
               "Verification File: '{}'\n" \
               "Report File: '{}'\n" \
               "Hashing: '{}'\n" \
               "".format(self.monitored_dir, self.verification_file, self.report_file, self.hashing)


class VerificationBase(Base):
    def __init__(
            self,
            monitored_dir: str,
            verification_file: str,
            report_file: str,
            hashing: str
    ):
        """Verification Base Model

        :param monitored_dir: str
            path of monitoring directory

        :param verification_file: str
            store result of verification files in this file

        :param report_file: str
            store report of every changes in this file

        :param hashing: str (default: 'sha1')
            Hashing function
            supported values: 'md5', 'sha1', 'sha256', 'sha512', 'sha224'
        """
        super(VerificationBase, self).__init__(monitored_dir, verification_file, report_file, hashing)

    def verify_verification_file(self, verification_file: str):
        """Verify Verification File
            Verify the specified file outside of monitored directory or not

        :param verification_file: str
            path of verification file
        """
        # Check is in monitored_dir
        assert self.monitored_dir not in verification_file, 'verification_file ' \
                                                            'should not be in monitored_dir'
        # Check verification file exist
        assert os.path.exists(verification_file), 'verification_file does not exist'
