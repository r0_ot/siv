from src.utils import load, Log, check_modification
from src.models import (
    FileDescriptorType,
    VerificationType,
    VerificationBase,
    Reporting,
    InitBase,
    Runnable,
    Base
)
import time
import pwd
import grp
import os


class SIV(Runnable):

    INIT_MODE = 'init'
    VERIFICATION_MODE = 'verification'
    SUPPORTED_MODES = [
        INIT_MODE,
        VERIFICATION_MODE,
    ]

    def __init__(
            self,
            monitored_dir: str,
            verification_file: str,
            report_file: str,
            hashing: str = Base.SHA1_HASH,
            mode: str = VERIFICATION_MODE,
            verbose: bool = True
    ):
        """System Integrity Verifier

        :param monitored_dir: str
            path of monitoring directory

        :param verification_file: str
            store result of verification files in this file

        :param report_file: str
            store report of every changes in this file

        :param hashing: str (default: 'sha1')
            Hashing function
            supported values: 'md5', 'sha1', 'sha256', 'sha512', 'sha224'

        :param mode: str (default: 'verification')
            mode of System Integrity Verifier
            supported modes: 'init', 'verification'

        :param verbose: bool
            verbose for each file
        """
        # Assign inputs to variables

        # Full path of verification file
        self.verification_file: str = os.path.abspath(verification_file)

        # Full path of monitored directory
        self.monitored_dir: str = os.path.abspath(monitored_dir)

        # Full path of report file
        self.report_file: str = os.path.abspath(report_file)

        # Selected hash function
        self.hashing: str = hashing

        # Verbose in running duration
        self.verbose: bool = verbose

        # Selected mode
        self.mode: str = mode
        if self.mode not in self.SUPPORTED_MODES:
            raise ValueError("supported modes: '{}'".format(self.SUPPORTED_MODES))

        # Perform tasks of each mode and initialize related model
        if self.mode == self.VERIFICATION_MODE:

            if os.path.exists(self.verification_file):

                # Load Pickle Verification file
                verification: VerificationType = VerificationType(
                    output=None, save=False, **load(self.verification_file)
                )

                # Restore Hash function
                self.hashing = verification.hashing

                # Restore File Descriptors
                self.file_descriptors = verification.file_descriptors

            else:
                raise FileNotFoundError(
                    "verification_file: '{}' doesn't exist".format(self.verification_file)
                )

            # Select VerificationBase model
            self.model: Base = VerificationBase(
                self.monitored_dir, self.verification_file, self.report_file, self.hashing
            )

        else:
            # Select InitBase Model
            self.model: Base = InitBase(
                self.monitored_dir, self.verification_file, self.report_file, self.hashing
            )

    def _init_mode(self):

        # Start time
        timer = time.time()

        # FIXME: non-standard solution !
        file_descriptors, n_dirs, n_files = dict(), 0, 0
        for (path, dirs, files) in os.walk(self.model.monitored_dir):
            for file in files:
                # Full Path file
                full_path = os.path.join(path, file)

                # Stat of file
                stat = os.stat(full_path)

                # read content of file
                content = open(full_path, 'rb').read()

                # File Descriptor
                fd = FileDescriptorType(
                    full_path=full_path,
                    size=stat.st_size,
                    owner=pwd.getpwuid(stat.st_uid).pw_name,
                    group=grp.getgrgid(stat.st_gid).gr_name,
                    permission=oct(stat.st_mode)[-3:],
                    last_modification=stat.st_mtime,
                    digest=self.model.hash_function(content).hexdigest()
                )

                # Add to a dictionary
                file_descriptors.update({fd.full_path: fd})

                # count number of files
                n_files += 1

            # count number of directories
            n_dirs += 1

        # Store Verification File
        VerificationType(
            output=self.model.verification_file,
            hashing=self.model.hashing,
            file_descriptors=file_descriptors
        )

        # Reporting
        Reporting(
            output=self.model.report_file,
            monitored_path=self.model.monitored_dir,
            verification_file=self.model.verification_file,
            n_dirs=n_dirs,
            n_files=n_files,
            timing=time.time() - timer
        )

    def _verification_mode(self):

        # Start time
        timer = time.time()

        # FIXME: non-standard solution !
        file_descriptors, n_dirs, n_files, report_messages = dict(), 0, 0, ''
        for (path, dirs, files) in os.walk(self.model.monitored_dir):
            for file in files:
                # Full Path file
                full_path = os.path.join(path, file)

                # Stat of file
                stat = os.stat(full_path)

                # read content of file
                content = open(full_path, 'rb').read()

                # File Descriptor
                fd = FileDescriptorType(
                    full_path=full_path,
                    size=stat.st_size,
                    owner=pwd.getpwuid(stat.st_uid).pw_name,
                    group=grp.getgrgid(stat.st_gid).gr_name,
                    permission=oct(stat.st_mode)[-3:],
                    last_modification=stat.st_mtime,
                    digest=self.model.hash_function(content).hexdigest()
                )

                # 1. store files in order to find removed files in future
                file_descriptors.update({fd.full_path: fd})

                # count number of files
                n_files += 1

                # 2. Check new file added or not
                selected: FileDescriptorType = self.file_descriptors.get(fd.full_path, None)
                if selected:
                    # 3. check file modified or not
                    comparison = fd.__cmp__(selected)
                    if comparison == FileDescriptorType.C_NOT_MODIFIED:
                        continue

                    report_messages += check_modification(selected, fd, comparison, self.verbose)
                else:
                    report_messages += "{}\n".format(Log.warn("New '{}' added.".format(fd.full_path)))

            # count number of directories
            n_dirs += 1

        # 4. find removed files
        removed_files = self.file_descriptors.keys() - file_descriptors.keys()
        for removed_file in removed_files:
            report_messages += "{}\n".format(Log.warn("File '{}' removed.".format(removed_file)))

        # Reporting
        Reporting(
            output=self.model.report_file,
            monitored_path=self.model.monitored_dir,
            verification_file=self.model.verification_file,
            n_dirs=n_dirs,
            n_files=n_files,
            message=report_messages,
            timing=time.time() - timer
        )

    def run(self):

        if self.mode == self.INIT_MODE:

            # initialization mode
            self._init_mode()

        elif self.mode == self.VERIFICATION_MODE:

            # verification mode
            self._verification_mode()
