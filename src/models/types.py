from typing import Dict
import pickle as pkl
import abc


class FileDescriptorType:

    C_NOT_MODIFIED = 0  # 0
    C_PERMISSION = 1 << 0  # 1
    C_DATE = 1 << 1  # 2
    C_OWNER = 1 << 2  # 4
    C_GROUP = 1 << 3  # 8
    C_HASH = 1 << 4  # 16
    C_SIZE = 1 << 5  # 32

    def __init__(
            self,
            full_path: str,
            size: int,
            owner: str,
            group: str,
            permission: str,
            last_modification: int,
            digest: str
    ):
        """Description of each File

        :param full_path: str
            full path of file

        :param size: int
            size of file in bytes

        :param owner: str
            owner of file

        :param group: str
            group of file

        :param permission: str
            right access of file in octal format

        :param last_modification: int
            timestamp of last modification

        :param digest: str
            hash of content of file
        """
        self.full_path: str = full_path
        self.size: int = size
        self.owner: str = owner
        self.group: str = group
        self.permission: str = permission
        self.last_modification: int = last_modification
        self.digest: str = digest

    def __cmp__(self, other):

        code = self.C_NOT_MODIFIED

        # Compare Size
        if not (self.size == other.size):
            code |= self.C_SIZE

        # Compare owner
        if not (self.owner == other.owner):
            code |= self.C_OWNER

        # Compare Group
        if not (self.group == other.group):
            code |= self.C_GROUP

        # Compare Permission
        if not (self.permission == other.permission):
            code |= self.C_PERMISSION

        # Compare Modification
        if not (self.last_modification == other.last_modification):
            code |= self.C_DATE

        # Compare Hashing
        if not (self.digest == other.digest):
            code |= self.C_HASH

        return code

    def __str__(self):
        return "(" \
               "full_path='{}', " \
               "size='{}', " \
               "owner='{}', " \
               "group='{}', " \
               "permission='{}', " \
               "last_modification='{}', " \
               "digest='{}'" \
               ")".format(
            self.full_path,
            self.size,
            self.owner,
            self.group,
            self.permission,
            self.last_modification,
            self.digest
        )


class _StoreType:

    def __init__(self, output: str):
        self.output: str = output

    @abc.abstractmethod
    def _store(self, *args):
        pass


class Reporting(_StoreType):

    def __init__(
            self,
            output: str,
            monitored_path: str,
            verification_file: str,
            n_dirs: int,
            n_files: int,
            timing: float,
            message: str = None,
            save: bool = True
    ):
        """Report Generator
            Generate report by passed arguments

        :param output: str
            write report in this file

        :param monitored_path: str
            monitored directory

        :param verification_file: str
            path of verification file

        :param n_dirs: int
            number of parsed directories

        :param n_files: int
            number of parsed files

        :param timing: float
            timestamp of time to complete

        :param message: str (default = None)
            every additional message that you want to write into report file

        :param save: bool (default: True)
            save model
        """
        super(Reporting, self).__init__(output)
        self.monitored_path: str = monitored_path
        self.verification_file: str = verification_file
        self.n_dirs: int = n_dirs
        self.n_files: int = n_files
        self.timing: float = timing
        self.message: str = message or '----------------[NOTHING]----------------'

        if save:
            # store
            self._store(
                self.monitored_path,
                self.verification_file,
                self.n_dirs,
                self.n_files,
                self.timing,
                self.message
            )

    def _store(self, *args):
        data = "Monitored Path: '{}'\n" \
               "Verification File: '{}'\n" \
               "Number of Parsed Directories: '{}'\n" \
               "Number of Parsed Files: '{}'\n" \
               "Time to Complete: '{}'\n" \
               "Messages: (I: info, W: warning, E: error)\n" \
               "{}" \
               "".format(*args)

        with open(self.output, 'w') as file:
            file.write(data)


class VerificationType(_StoreType):
    def __init__(
            self,
            output: str,
            hashing: str,
            file_descriptors: Dict,
            save: bool = True,
    ):
        """Verification Model

        :param output: str
            write verification to output file

            we store verification by follow template:
            {
                'hashing': HASHING,
                'files_descriptors': FILE_DESCRIPTORS
            }

        :param hashing: str
            hashing function that restored after load verification file

        :param file_descriptors: Dict
            represent a file descriptor as follow:
            {
                full_path: FileDescriptor,
                ...
            }

        :param save: bool (default: True)
            save model
        """
        super(VerificationType, self).__init__(output)

        self.file_descriptors: Dict = file_descriptors
        self.hashing: str = hashing

        self.template: dict = dict(
            hashing=self.hashing,
            file_descriptors=self.file_descriptors
        )

        if save:
            # store
            self._store(self.template)

    def _store(self, args):
        pkl.dump(args, open(self.output, 'wb'))
