from setuptools import setup
from os import path

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md')) as f:
    long_description = f.read()


setup(
    name='siv',
    version='0.4',
    packages=['src', 'src.utils', 'src.models'],
    url='https://gitlab.com/r0_ot/siv',
    license='GPLv3',
    author='Alireza Km',
    author_email='alitm28@gmail.com',
    description='System Integrity Verifier',
    long_description=long_description,
    long_description_content_type='text/markdown',
    keywords=['siv', 'security', 'system', 'integrity', 'verifier'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'siv=run'
        ]
    },
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
    ],
)
